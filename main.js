document.getElementById('task-form').addEventListener('submit', addTask);

function addTask(e) {
    e.preventDefault();
    const taskInput = document.getElementById('task-input');
    const taskList = document.getElementById('task-list');
    const taskCount = document.getElementById('task-count');

    if (taskInput.value === '') return;

    const task = document.createElement('li');
    task.classList.add('flex', 'justify-between', 'p-2', 'border', 'rounded', 'bg-white');

    const title = document.createElement('span');
    title.textContent = taskInput.value;
    task.appendChild(title);

    const deleteButton = document.createElement('button');
    deleteButton.innerHTML = '<i class="fas fa-trash"></i>';
    deleteButton.classList.add('text-red-500');
    deleteButton.addEventListener('click', deleteTask);
    task.appendChild(deleteButton);

    taskList.appendChild(task);
    taskInput.value = '';

    updateTaskCount();
}

function deleteTask(e) {
    e.target.parentElement.remove();
    updateTaskCount();
}

function updateTaskCount() {
    const taskCount = document.getElementById('task-count');
    const taskList = document.getElementById('task-list');
    taskCount.textContent = `${taskList.children.length} tasks left`;
}
